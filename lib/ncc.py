import collections

def addfunc(caller_func, new_callee, funcs):
    if caller_func not in funcs:
        funcs[caller_func]

    try:
        funcs[caller_func].add(new_callee)
    except:
        print("==========")
        print("caller_func: {0} new_callee: {1}".format(caller_func, \
                                                        new_callee))
        print("==========")

    return funcs

def rmfunc(caller_func, rm_callee, funcs):
    funcs[caller_func] = {x for x in funcs[caller_func] if x != rm_callee}
    return funcs

def word(sword):
    return sword.rstrip(" ()\t\n")

def read_ncc_file(filename):
    cfunc = None    #current func
    funclist = []   # actual functions -- this excludes place holders for
                    # function pointers, but includes the functions point to
                    # by function pointers.
    rfunc = collections.defaultdict(set)

    # funcs: function dictionary
    # this dictionary maps a function to a list containing dependencies
    funcs = collections.defaultdict(set)

    with open(filename) as infile:

        # process lines in nccout file
        # and populate dictionary
        for line in infile:
            if len(line) == 0:
                pass
            elif line[0] == 'D': # CALLING FUNCTION
                cfunc = word(line[3:])
                try:
                    if (not cfunc[0] is '*') and (not '/' in cfunc) \
                        and (cfunc not in funclist):
                        funclist.append(cfunc)
                except:
                    print ("FAILED: {0}".format(line))
                funcs[cfunc]
            elif line[0] == 'F': # CALLED FUNCTION
                w = word(line[3:])
                addfunc(cfunc, w, funcs)
            elif line[0] == 'R':
                l = line[3:].split()
                # function set l[1] called via the function pointer l[0]
                if word(l[0]) in rfunc:
                    rfunc[word(l[0])].add(word(l[1]))
                else:
                    rfunc[word(l[0])] = set([word(l[1])])
                cfunc = None

    return funclist, funcs, rfunc

def print_cfg(funcs):
    for func in funcs:
        print("D: {0}".format(func))
        for c in funcs[func]:
            print("F: {0}".format(c))
        print("")
