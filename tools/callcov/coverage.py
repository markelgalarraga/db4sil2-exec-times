
class coverage:
    def __init__(self):
        self.called = 0
        self.total = 0

    def set_called(self, called):
        self.called = called

    def inc_called(self):
        self.called += 1

    def get_called(self):
        return self.called

    def set_total(self, total):
        self.total = total

    def get_total(self):
        return self.total

    def get_percentage(self):
        if self.total > 0:
           return (self.called/self.total)*100
        else:
           return 0
