#!/usr/bin/python3

import os
import sys
import collections
import coverage
from lib import ncc

### helper functions ###
def read_systemmap(filename):
    symtab = set()

    with open(filename) as fmap:
        for line in fmap:
            # just take function names + remove trailing '\n'
            symtab.add(line.split(" ")[2][:-1])

    return symtab # fust a list of functions in the System.map

def funcname_from_tline(tline):
    funcname = tline.strip()[:-2].split(" [")[0]
    return funcname

def tracetocfg(cfg, tracefile):
    levels = []

    with open(tracefile) as fin:
        for line in fin:
#          print("Handling line: {0}".format(line[:-1]))

            cur_spaces = line.count(" ")

            funcname = funcname_from_tline(line)

            if len(levels) == 0:
                levels.append(funcname)
            elif int(cur_spaces/2) + 1 == len(levels):
                levels.pop()
                levels.append(funcname)
                cfg[levels[len(levels)-2]].add(funcname)
            elif int(cur_spaces/2) + 1 > len(levels):
                cfg[levels[len(levels)-1]].add(funcname)
                levels.append(funcname)
            elif int(cur_spaces/2) + 1 < len(levels):
                while int(cur_spaces/2) != len(levels):
                    levels.pop()
                levels.append(funcname)
                cfg[levels[len(levels)-2]].add(funcname)


def walk_back_tree(cov, stat_cfg, caller, called_func):
    plist = stat_cfg[caller]
    found = False
    rounds = 4
    r = 0

    while not found and len(plist) >= 0 and r <= rounds:
        r += 1
#        print(".")
        for p in plist:
#            print("checking: {0}".format(p))
            if called_func in stat_cfg[p]:
                cov[caller].inc_called()
                cov[p].inc_called()
                found = True
                print("Found Call: {0} -> ? -> {1}".format(caller, called_func))

        cur_list = plist
        plist = set()
        for p in cur_list:
#           print("x")
            plist |= stat_cfg[p]


def check_calls(cov, dyn_cfg, stat_cfg, caller):
    for called in dyn_cfg[caller]:
        if called in stat_cfg[caller]:
            cov[caller].inc_called()
        else:
            walk_back_tree(cov, stat_cfg, caller, called)
            print("subset assumption vilolated!")


### main ###
if __name__ == '__main__':

#
# 1. prepare the static_cfg -- that is the control-flow graph produced by
#    static code analysis, reduced by inlined functions based on the System.map
#
    flist, static_cfg, resolve_funcs = ncc.read_ncc_file(sys.argv[1])

#    symtab = read_systemmap(sys.argv[2])

#    inline_funcs = []

#    for f in flist:
#        if f not in symtab:
#            inline_funcs.append(f)

#    for i in inline_funcs:
#        called_funcs = static_cfg[i]
#        flist.remove(i)
#        del static_cfg[i]
#        if len(called_funcs) > 0: # does this function call further functions?
#            for pc in flist:
#                if i in static_cfg[pc]:
#                    for ctmp in called_funcs:
#                        if ctmp != None:
#                            static_cfg = addfunc(pc, ctmp, static_cfg)
#                    static_cfg = rmfunc(pc, i, static_cfg)

#
# 2. prepare the dynamic_cfg -- that is the control-flow graph produced by
#    converting tracing data into a cfg.
#
    # create cfg from tracing data
    dynamic_cfg = collections.defaultdict(set)

    for f in os.listdir(sys.argv[2]):
        tracetocfg(dynamic_cfg, sys.argv[2]+f)

#    print_cfg(dynamic_cfg)

#
# 3. compare static_cfg and dynamic_cfg and get a coverage.
#
    # get coverage
    cov = collections.defaultdict(coverage.coverage)
    for cf in dynamic_cfg:
        print("Handling: {0}".format(cf))
        if static_cfg[cf]:
            cov[cf].set_total(len(static_cfg[cf]))
        else:
            print("caller {0} in dynamic_cfg but not in static_cfg!".format(cf))

#        print("Comparing: \n static_cfg: {0}\n dynamic_cfg: {1}".format(dynamic_cfg[cf], static_cfg[cf])) 
        check_calls(cov, dynamic_cfg, static_cfg, cf)

        print("Called {0}/{1} ({2}%)".format(cov[cf].get_called(), cov[cf].get_total(), cov[cf].get_percentage()))
        print("========================================")

    overall_total = 0
    overall_called = 0
    for func_cov in cov:
        overall_total += cov[func_cov].get_total()
        overall_called += cov[func_cov].get_called()

    print("OVERALL RESULTS:\nCalled {0}/{1} ({2}%)".format(overall_called, overall_total, (overall_called / overall_total) * 100))
