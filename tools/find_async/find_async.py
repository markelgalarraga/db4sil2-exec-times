#!/usr/bin/python3

import os
import sys
import collections
from lib import ncc

maxdepth=1000
shown = []

# recursively trace callback path
def gencbgraph(caller, callee, funcs, rfunc, d):

    # write path that traverse function pointer
    if callee not in rfunc:
        if d < maxdepth and callee not in shown:
            gengraph(callee, funcs, rfunc, d + 1)

        return

    # trace function pointers until entity appears
    for nextCallee in rfunc[callee]:
        gencbgraph(caller, nextCallee, funcs, rfunc, d)

def gengraph(s, funcs, rfunc, d=0):
    if s:
        shown.append(s)
    if s in funcs:
        l = funcs[s]
        for f in l:
            if f[0] == '*':
                # recursively trace callback path via the function pointer f[0]
                gencbgraph(s, f, funcs, rfunc, d)
                continue
#            print '"%s" -> "%s";' % (s, f)
            if d < maxdepth and f not in shown:
                gengraph(f, funcs, rfunc, d + 1)

##########



### main ###
if __name__ == '__main__':

    funclist, funcs, rfuncs = ncc.read_ncc_file(sys.argv[1])

    gengraph(sys.argv[2], funcs, rfuncs)
   

    subgraph = collections.defaultdict(set)
    for node in shown:
        subgraph[node] = funcs[node]

    ncc.print_cfg(subgraph)

    fbuf = []
    for f in os.listdir(sys.argv[3]):
        with open(sys.argv[3]+f) as fin:
            for line in fin:
                fbuf.append(line.strip())

#    print(fbuf)
    print(shown)

    for traced_func in fbuf:
        if traced_func[:-2].split(" [")[0] not in shown:
            print("{0} in trace but not in cfg.".format(traced_func[:-2].split(" [")[0]))
