"""
test_server.py creates a number of worker threads that do post-processing of
raw tracing data collected by test_client.py.
"""
import threading, queue
import db4sil2config as cfg
import socket
import os
import signal
import test_worker
import logger as l
import collections
import dill as pickle
import time

EXIT_FLAG = 0

def sig_handler(signum, frame):
    """
    signal handler for SIGUSR1 -- the signal to stop the server.
    """
    print("Shutting down DB4SIL2 Test Server.")
    EXIT_FLAG = 1

def check_dump_file(dumpfile, logger_queue):
    """
    if a dump of the internal stat of the last run exists
    the load it -- this way we already know the hashes of the
    existing traces!
    """
    slist = collections.defaultdict(lambda: collections.defaultdict(list))

    if os.path.isfile(dumpfile):
        with open(dumpfile, 'rb') as fdump:
            slist = pickle.load(fdump)
            msg = "Restored slist state: {0}".format(slist)
            l.log_a_msg(msg, "INFO", logger_queue, "test_server")

    return slist

def init_test_server(config_file):
    """
    Init test_server -- read config file, start logger and workers
    """
    threadid = 1
    tids = []

    tracefile = ""

    trace_queue = queue.Queue()
    logger_queue = queue.Queue()

    signal.signal(30, sig_handler)

    syscalls, \
    nr_workers, \
    hostname, \
    numruns, \
    del_raw = cfg.get_postprocessing_config(config_file, "test_server", \
                                            logger_queue)

    host = '' # Symbolic name meaning all available interfaces
    port = cfg.get_server_config(config_file, "test_server", logger_queue)

    # Before we do anything else: start the logger thread, so we can log
    # whats going on
    tid = l.DB4SIL2Logger(threadid, "logger", \
                               logger_queue, "db4sil2_server.log")
    tid.daemon = True # make the logger thread a daemon, this way the main
                           # will clean it up before terminating!
    tid.start()
    tids.append(tid)
    threadid += 1

    slist = check_dump_file('server_internal.dump', logger_queue)

    while threadid-1 <= nr_workers:
        tname = "Worker" + str(threadid-1)
        tid = test_worker.Testworker(tname, trace_queue, logger_queue, \
                                     syscalls, slist, hostname, numruns, \
                                     del_raw)
        tid.daemon = True
        tid.start()
        tids.append(tid)
        threadid += 1

    l.log_a_msg("All threads started successfully.", "INFO", \
                logger_queue, "test_server")

    return host, port, tracefile, slist, trace_queue, logger_queue, numruns

def wait_for_raw_traces(host, port, tracefile, trace_queue, logger_queue, numruns):
    """
    receive info about raw_traes ready to handle via a socket and put it
    into the trace_queue so workers can pick them up for post-processing.
    """
    run = 0
    while not EXIT_FLAG:
        # If camapign is over, do not let the client to start with the next one
        if run >= numruns:
            #l.log_a_msg("Ignoring client, numruns reached", "INFO", logger_queue, "test_server")
            time.sleep(5)
        else:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.bind((host, port))
                sock.listen(1)
                try:
                    conn, addr = sock.accept()
                except OSError as err:
                    l.log_a_msg(err, "ERROR", logger_queue, "test_server")
                    break

                conn, addr = sock.accept() # we connect again (Linux has timed the conn out)
                tracefile = conn.recv(1024)
                l.log_a_msg("Host {0} Request for file {1}".format(addr, \
                            tracefile.decode("utf-8")), "INFO", logger_queue, \
                            "test_server")

                if run < numruns:
                    trace_queue.put(dict({"tfile": tracefile.decode("utf-8")}))
                    l.log_a_msg("Current Queue Size: {0}, Run Number: {1}".format(trace_queue.qsize(), run), \
                            "INFO", logger_queue, "test_server")
                    run += 1

                conn.sendall(b'1') # Notify the client that it can close the socket
                sock.close()
                time.sleep(1) # para que no tengamos "address already in use"


def server_code():
    """
    test_server
    """

    config_file = 'db4sil2.cfg'
    host, port, tracefile, \
    slist, trace_queue, logger_queue, numruns = init_test_server(config_file)

    wait_for_raw_traces(host, port, tracefile, trace_queue, logger_queue, numruns)

    # EXIT_FLAG was set
    # let's store our internal state to a file so we may resume
    # from here:
    with open('server_internal.dump', 'wb') as fdump:
        pickle.dump(slist, fdump, pickle.HIGHEST_PROTOCOL)

    l.log_a_msg("test_server shutdown.", "INFO", logger_queue, "test_server")


if __name__ == '__main__':
    server_code()
