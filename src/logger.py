########################################################################
# logger.py -- log in a common format to a file.
#
# Copyright (C) 2015 Andreas Platschek <andi.platschek@gmail.com>
# License GPL V2 or later (see http://www.gnu.org/licenses/gpl2.txt)
########################################################################
"""
logger.py -- log in a common format to a file.
"""
import threading
import logging

def log_a_msg(logmsg, logtype, loggerq, loggername):
    """
    Helper funciton for logging a message.
    """
    loggerq.put(dict({"msg": logmsg, "type": logtype, \
                      "loggername": loggername}))

class DB4SIL2Logger(threading.Thread):
    """
    The Logger Thread itself takes log messages from the logger queue
    and writes them into the log-file
    """
    def __init__(self, thread_id, name, logger_queue, logfile):
        threading.Thread.__init__(self)
        self.name = name
        self.thread_id = thread_id
        self.logger_queue = logger_queue
        self.logfile = logfile

    def run(self):
        logformat = ("%(asctime)-15s [%(loggername)-12s] "
                     "%(levelname)s: %(logmsg)s")
        logging.basicConfig(format=logformat, filename=self.logfile, \
                            level=logging.DEBUG)

        while True:
            next_log_msg = self.logger_queue.get(True)

            msgf = dict({"loggername":str(next_log_msg.get('loggername')), \
                      "logmsg":str(next_log_msg.get('msg'))})

            if str(next_log_msg.get('type')) == "DEBUG":
                logging.debug("%s ", extra=msgf)
            elif str(next_log_msg.get('type')) == "INFO":
                logging.info("%s ", extra=msgf)
            elif str(next_log_msg.get('type')) == "WARNING":
                logging.warning("%s ", extra=msgf)
            elif str(next_log_msg.get('type')) == "ERROR":
                logging.error("%s ", extra=msgf)
            else: #if we don't know, we assume the worst
                logging.error("%s ", extra=msgf)
