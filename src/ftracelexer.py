# ------------------------------------------------------------
# ftracelexer.py
#
# tokenizer for ftrace output
# ------------------------------------------------------------

# Some pylint warnings need to be disabled due to how PLY works:
#    1. PLY requires a certain naming (e.g. t_ANY_ ) which is not accepted by
#       pylint, thus the warning "invalid-name" is disbled for this entire file.
#    2. Most functions could be moved outside the class as they don't use self,
#       I still would like to keep everypart of the lexer in the class so I
#       also disabled "no-self-use".
#    3. PLY uses docstring info to specify the regex for which the function is
#       executed, thus we can't use it for documentation here, thus
#       "missing-docstring" is also disabled.
#
#pylint: disable=invalid-name, no-self-use, missing-docstring

import re
import logger as l

class Ftracelexer:
    """
    a PLY lexer to tokenize ftrace raw traces.
    """

    states = (
        ('insyscall', 'exclusive'),
        ('asyncevent', 'exclusive'),
    )

    # List of token names.   This is always required
    tokens = (
        'TPID',
        'CALLSTART',
        'CALLEND',
        'CALLSTARTEND',
        'INTSTART',
        'INTEND',
        'BRAOPEN',
        'BRACLOSE',
        'COMMENT',
        'TLINE',
        'ASYNCSTART',
        'ASYNCEND',
        'TSWITCH',
    )

    # Comments in the raw trace are discarded.
    t_ANY_ignore_COMMENT = r'\/\*[\w\[\] .]*\*\/'

    t_ANY_TSWITCH = r'\#\#TASK_SWITCH:\s*[\w<>.\-]*\s*\=>\s*[\w<>.\-]*\s*\#\#'

    def t_ANY_TPID(self, token):
        r'\d\)\s*[\w<>\-.]+-\d{1,5}\s'
        # If the PID changed, we need to switch to using the data of the
        # new PID:
        if token.lexer.currentpid != token.value:
            token.lexer.trace_state[token.lexer.currentpid] = \
                        token.lexer.current_state()
            token.lexer.currentpid = token.value
            if not token.lexer.trace_state[token.lexer.currentpid]:
                token.lexer.trace_state[token.lexer.currentpid] = "INITIAL"
            token.lexer.begin(token.lexer.trace_state[token.lexer.currentpid])
            token.value = token.value.split(")")[1].strip()
            if not token.lexer.num_braces[token.lexer.currentpid]:
                token.lexer.num_braces[token.lexer.currentpid] = 0
            return token

    def t_INITIAL_CALLSTART(self, token):
        if ";" in token.value:
            token.type = "CALLSTARTEND"
        else:
            token.lexer.num_braces[token.lexer.currentpid] = 0
            token.lexer.begin('insyscall')
        return token

    def t_insyscall_CALLSTART(self, token):
        l.log_a_msg("Found new syscall while in syscall!", "ERROR", \
                    self.loggerq, self.workername)
        return token

    def t_asyncevent_CALLSTART(self, token):
        l.log_a_msg("Found new syscall while in asyncronous event!", "ERROR", \
                  self.loggerq, self.workername)
        return token

    def t_insyscall_ASYNCSTART(self, token):
        (r'__do_page_fault\(\);?'
         r'|__update_curr\(\);?'
         r'|__compute_runnable_contrib\(\);?'
         r'|preempt_schedule_irq\(\);?'
         r'|tty_schedule_flip\(\);?'
         r'|smp_reschedule_interrupt\(\);?')
        curpid = token.lexer.currentpid
        token.lexer.async_balance[curpid].append(token.lexer.num_braces[curpid])
        token.lexer.async_nesting[token.lexer.currentpid] += 1
        token.lexer.begin('asyncevent')
        return token

    def t_insyscall_INTSTART(self, token):
        r'\#\#INTERRUPT_START\#\#'
        token.lexer.begin('asyncevent')
        return token

    # TODO: Handle interrupts outside of a syscall -- for now
    #       those are ignored.
    def t_INITIAL_INTSTART(self, token):
        r'\#\#INTERRUPT_START\#\#'

    def t_asyncevent_INTEND(self, token):
        r'\#\#INTERRUPT_END\#\#'
        token.lexer.begin('insyscall')
        return token

    # TODO: Handle interrupts outside of a syscall -- for now
    #       those are ignored.
    def t_INITIAL_INTEND(self, token):
        r'\#\#INTERRUPT_END\#\#'

    t_asyncevent_INITIAL_ignore_TLINE = r'[\w.\[\] ]+\(\);?'
    def t_insyscall_TLINE(self, token):
        r'[\w.\[\] ]+\(\);?'
        # some lines have a semicolon, some donet -- drop it everywhere
        token.value = re.sub(";", "", token.value)
        return token

    t_INITAL_ignore_BRAOPEN = r'\{'
    def t_insyscall_asyncevent_BRAOPEN(self, token):
        r'\{'
        token.lexer.num_braces[token.lexer.currentpid] += 1
        token.value = (token.value, \
                       token.lexer.num_braces[token.lexer.currentpid])
        return token

### The followint two (t_CALLEND() and t_ASYNCEND() should never be a match,
### they are here to satisfy the lexer. The actual tokens CALLEND and ASYCNEND
### are generated in t_BRACLOSE -- they are conditionals based on the nesting
### level of braces!
    def t_insyscall_CALLEND(self, token):
        r'NEVER EVER ACTUALLY MATCH THIS RULE! ONLY CALLED BY t_BRACLOSE!'
        return token

    def t_asyncevent_ASYNCEND(self, token):
        r'NEVER EVER ACTUALLY MATCH THIS RULE! ONLY CALLED BY t_BRACLOSE!'
        return token

    t_INITIAL_ignore_BRACLOSE = r'\}'
    def t_insyscall_asyncevent_BRACLOSE(self, token):
        r'\}'
        curpid = token.lexer.currentpid

        token.lexer.num_braces[curpid] -= 1

        if 0 == token.lexer.num_braces[curpid]:
            token.type = "CALLEND"
            token.lexer.begin('INITIAL')
        elif 0 > token.lexer.num_braces[curpid]:
            token.lexer.num_braces[curpid] = 0

        #check if async event ends:
        curnumbraces = token.lexer.num_braces[curpid]
        asyncnestlevel = token.lexer.async_nesting[curpid]

        if asyncnestlevel > 0 and \
           curnumbraces == token.lexer.async_balance[curpid][asyncnestlevel-1]:

            token.lexer.async_nesting[curpid] -= 1
            token.lexer.async_balance[curpid].pop()
            token.type = "ASYNCEND"
            token.lexer.begin('insyscall')

        token.value = (token.value, token.lexer.num_braces[curpid])
        return token

    # Define a rule so we can track line numbers
    def t_ANY_newline(self, token):
        r'\n+'
        token.lexer.lineno += len(token.value)

    # A string containing ignored characters (spaces and tabs)
    t_ANY_ignore = ' \t'

    # An error handling rule, in case no regex could be matched.
    def t_ANY_error(self, token):
        l.log_a_msg("Illegal character '%s'" % token.value[0], "ERROR", \
                  self.loggerq, self.workername)
        token.lexer.skip(1)

    def __init__(self, searchstring, workername, loggerq):
        self.t_INITIAL_CALLSTART.__func__.__doc__ = searchstring
        self.t_insyscall_CALLSTART.__func__.__doc__ = searchstring
        self.t_asyncevent_CALLSTART.__func__.__doc__ = searchstring
        self.loggerq = loggerq
        self.workername = workername
