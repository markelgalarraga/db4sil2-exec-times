"""
Helper functions for parsing the configuration file.
"""

import configparser
import re
import logger as l

def get_test_config(configfile, tname, loggerq):
    """
    returns all information on tests, this includes:
        - list of test executables
	- ftrace path
        - number of test runs
        - testmode (suiteiteration vs. testiteration)
        - maximum allowed backlog before starting another round of tests.
    """
    tracingpath = "/sys/kernel/tracing"
    testexec = None
    testfile = None
    testexec_list = []
    num_runs = 1
    testmode = "testiteration"
    backlog = 200

    try:
        config = configparser.ConfigParser()
        config.readfp(open(configfile))

        check = config.has_option('general', 'tracingpath')
        if check:
            tracingpath = config.get('general', 'tracingpath')

        check = config.has_option('tests', 'testexec')
        if check:
            testexec = config.get('tests', 'testexec')

        check = config.has_option('tests', 'testexec_files')
        if check:
            testfiles = config.get('tests', 'testexec_files')

        check = config.has_option('tests', 'num_runs')
        if check:
            num_runs = int(config.get('tests', 'num_runs'))

        check = config.has_option('tests', 'testmode')
        if check:
            testmode = config.get('tests', 'testmode').strip()

        check = config.has_option('tests', 'allowed_backlog')
        if check:
            backlog = int(config.get('tests', 'allowed_backlog'))

    except configparser as err:
        l.log_a_msg(err, "ERROR", loggerq, tname)

    if None != testexec:
        testexec_list.append(testexec)

    if None != testfiles:
        for testfile in testfiles.split(" "):
            with open(testfile) as fp_test:
                for line in fp_test:
                    testexec_list.append(line.lstrip().rstrip())

    return testexec_list, tracingpath, num_runs, testmode, backlog

def get_postprocessing_config(configfile, tname, loggerq):
    """
    get post-processing related configuration data, this includes:
        - A list of system calls that shall be considered,
        - the number of worker threads that shall be started,
        - the hostname of the client that produced the traces,
        - a flag that indicates whether or not raw traces shall be
          deleted after processing (or be kept for debugging).
    """
    syscalls_list = []
    nr_workers = 3
    del_raw = True
    hostname = "noone"

    try:
        config = configparser.ConfigParser()
        config.readfp(open(configfile))

        check = config.has_option('general', 'hostname')
        if check:
            hostname = config.get('general', 'hostname')

        check = config.has_option('postprocessing', 'syscalls')
        if check:
            syscalls_raw = re.sub('\n', '', config.get('postprocessing', \
                                                       'syscalls'))
            syscalls_rawlist = syscalls_raw.split(',')
            for syscall in syscalls_rawlist:
                syscalls_list.append(str(syscall).rstrip().lstrip())

        check = config.has_option('postprocessing', 'nr_workers')
        if check:
            nr_workers = int(config.get('postprocessing', 'nr_workers'))

        if check:
            num_runs = int(config.get('tests', 'num_runs'))

        check = config.has_option('postprocessing', 'delete_raw_data')
        if check:
            del_raw = int(config.getboolean('postprocessing', \
                                            'delete_raw_data'))

    except configparser as err:
        l.log_a_msg(err, "ERROR", loggerq, tname)

    return syscalls_list, nr_workers, hostname, num_runs, del_raw


def get_client_config(configfile, tname, loggerq):
    """
    The test_client needs a servername and server to which it shall
    connect and pass finished raw traces to.
    """
    servername = "localhost"
    serverport = 50007

    try:
        config = configparser.ConfigParser()
        config.readfp(open(configfile))

        check = config.has_option('network', 'servername')
        if check:
            servername = config.get('network', 'servername')
        check = config.has_option('network', 'serverport')
        if check:
            serverport = int(config.get('network', 'serverport'))

    except configparser as err:
        l.log_a_msg(err, "ERROR", loggerq, tname)

    return servername, serverport

def get_server_config(configfile, tname, loggerq):
    """
    The test_server only needs a port to listen to as network configuration
    this info is returned by get_server_config().
    """
    serverport = 50007

    try:
        config = configparser.ConfigParser()
        config.readfp(open(configfile))

        check = config.has_option('network', 'serverport')
        if check:
            serverport = int(config.get('network', 'serverport'))

    except configparser as err:
        l.log_a_msg(err, "ERROR", loggerq, tname)

    return serverport
