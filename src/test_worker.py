"""
test_worker.py implements the worker threads used for post-prcessing
               the raw traces collected by the test_client
"""
import threading
import process_trace
import datetime
import os
import signal

executed_runs = 0

class Testworker(threading.Thread):
    """
    initialze the worker thread.
    """
    #pylint: disable=too-many-arguments
    def __init__(self, name, trace_queue, log_queue, syscalls, \
                 slist, hostname, num_runs, del_raw):
        threading.Thread.__init__(self)
        self.name = name
        self.loggerq = log_queue
        self.traceq = trace_queue
        self.syscalls = syscalls
        self.slist = slist
        self.hostname = hostname
        self.del_raw = del_raw
        self.num_runs = num_runs

    def log_a_msg(self, msg, loglevel):
        """
        Helper function to write log messages into the log queue.
        """
        self.loggerq.put(dict({"msg": msg, "type": loglevel, \
                               "loggername": self.name}))

    def run(self):
        """
        The thread code of the worker thread.
        """
        self.log_a_msg("Starting Worker!", "INFO")

        results_dir = "/media/imanol/markel_trazas/carla_aeb_20230315/"

        global executed_runs

        search_list = ""
        for syscall in self.syscalls:
            search_list = r"{0}|{1}\(\);?".format(search_list, syscall)

        while True:
            # Block on the job queue
            tjob = self.traceq.get(True)
            tracefile = str(tjob.get('tfile'))

            self.log_a_msg("Handling Trace:{0}".format(tracefile), "INFO")

            testname = process_trace.strip_testname(tracefile)

            found_syscalls = process_trace.search_syscalls(tracefile, \
                                                           search_list[1:], \
                                                           testname, \
                                                           self.hostname, \
                                                           self.name, \
                                                           self.loggerq)

            process_trace.check_if_redundant(found_syscalls, \
                                             self.slist, \
                                             self.del_raw)

            self.log_a_msg("Trace Done:{0}".format(tracefile), "INFO")

            if self.del_raw:   # raw tracing data may be kept for debugging
                os.system("rm {0}".format(tracefile))

            executed_runs += 1

            if executed_runs == self.num_runs:
                current_t = datetime.datetime.now()
                cmd = "mkdir {0}/test_{1}; \
                       mv test_trace* {0}/test_{1}; \
                       mv syscalls {0}/test_{1};".format(results_dir, \
                                                            current_t.strftime("%Y-%m-%d_%H%M%S%f"))
                os.system(cmd)
                os.kill(os.getpid(), signal.SIGKILL)
