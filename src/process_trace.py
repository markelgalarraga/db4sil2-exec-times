"""
Helper functions to process ftrace output.
"""

import re
import datetime
import os
import hashlib
import collections
import ply.lex as lex
import ftracelexer
import logger as l

# reused function from http://stackoverflow.com/questions/3431825/generating-a-md5-checksum-of-a-file/3431838#3431838
def md5(fname):
    """
    Calculate MD5 sum of a file with name fname.
    """
    thash = hashlib.md5()
    with open(fname, "rb") as filep:
        for chunk in iter(lambda: filep.read(4096), b""):
            thash.update(chunk)
    return thash.hexdigest()

def generate_filenames(found_syscall, hostname):
    """
    Names of raw traces/timings/meta data are mixture of current time stamp
    and hostname.
    """
    current_t = datetime.datetime.now()
    outtracefile = "syscalls/traces/{0}/{1}".format(found_syscall, \
                                       current_t.strftime("%Y-%m-%d_%H%M%S%f"))
    timingfile = "syscalls/timings/{0}/{1}_{2}".format(found_syscall, \
                                       hostname, \
                                       current_t.strftime("%Y-%m-%d_%H%M%S%f"))
    metafile = "syscalls/meta/{0}/{1}_{2}".format(found_syscall, \
                                       hostname, \
                                       current_t.strftime("%Y-%m-%d_%H%M%S%f"))
    return outtracefile, timingfile, metafile

def write_buf_to_file(filename, buf):
    """
    Write the buffer (trace, meta, etc.) to a given file.
    """
    with open(filename, "w") as filep:
        for line in buf:
            filep.write(line)

def check_if_redundant(filelist, slist, del_raw):
    """
    If the hash of this traceing data is unknown we found a new unique trace.
    If it is already known we don't store it, since it is not unique.
    """
    for tfile in filelist:
        fhash = md5(tfile)
        syscall = tfile.split('/')[2]
        unique_name = tfile.split('/')[-1]

        if fhash in list(slist[syscall]):
            if del_raw:
                os.system("rm {0}".format(tfile))
        else:
            slist[syscall][fhash].append(unique_name)
            cmd = "cp {0} syscalls/traces/{1}".format(tfile, syscall)
            cmd = "{0}/unique_traces/{1}".format(cmd, fhash)
            os.system(cmd)
            if del_raw:
                os.system("rm {0}".format(tfile))
    return


def write_outfile(outfile, outbuf):
    """
    Write content of outbuf into outfile.
    """
    with open(outfile, "w") as fp_out:
        for tline in outbuf:
            fp_out.write(tline)

def read_tracebuf(tfile, workername, loggerq):
    """
    Read the raw trace from tfile and store it in a buffer. In addition
    certain events like task switches and interrupts are marked to simplyfy the
    processing later on.

    Markel
    Also save the durations column from the trace, adding an empty line every time
    a marked event is added to the trace buffer, so that the line number is
    maintained and can be used later on.
    """
    try:
        fp_trace = open(tfile)
    except OSError as err:
        l.log_a_msg("Unable to open tracefile: {0}".format(tfile), "ERROR", \
                    loggerq, workername)
        l.log_a_msg(err, "ERROR", loggerq, "ftrace_session")
        return 1

    tracebuf = ''
    # Markel
    timingbuf = ''

    for line in fp_trace:
        # skip comments and dash-lines
        if re.search("^#", line) or re.search("^\n$", line):
            #continue
            tracebuf += "\n"
            timingbuf += "\n"
        elif re.search("-----------", line):
            line = next(fp_trace)
            tracebuf += "##TASK_SWITCH: {0}##\n\n\n".format(\
                                                 line.split(')')[1].strip())
            # Markel
            # Task switches are preceded and succeeded by dashed lines,
            # but only one line is added to the trace buffer, therefore only
            # one line must be added to the timing buffer so that line
            # numbers are mantained 
            timingbuf += "\n\n\n"

            line = next(fp_trace)
        elif "====>" in line:
            tracebuf += "##INTERRUPT_START##\n"
            # Markel
            timingbuf += "\n"

        elif "<====" in line:
            tracebuf += "##INTERRUPT_END##\n"
            # Markel
            timingbuf += "\n"

        else:
            try:
                columns = line.split('|')
                #timingbuf.append(columns[1])
                # Markel
                #TODO no es necesario que sea string, puedo hacer lista
                timingbuf += columns[1] + "\n"

                tracebuf += columns[0]
                tracebuf += columns[2][2:]
            except IndexError:
                l.log_a_msg("LINE CAUSED A PROBLEM: '{0}'".format(line), \
                            "WARNING", loggerq, workername)

    fp_trace.close()
    #return tracebuf

    # Markel
    return tracebuf, timingbuf


# tracefile: the raw tracefile
# syscalls: the syscalls that shall be extracted
# testname: the name of the testexecutable that produced this trace
def search_syscalls(tracefile, searchlist, testname, hostname, \
                    workername, loggerq):
    """
    search syscalls in a raw trace file and store them in separate files
    """

    pt_meta_file = "syscalls/meta/PER_TEST/{0}_{1}".format(hostname, tracefile)

    try:
        fp_tmeta = open(pt_meta_file, "w")
        fp_tmeta.write("{0}\n".format(testname))
    except OSError as err:
        l.log_a_msg(err, "ERROR", loggerq, "ftrace_session")
        return 1

    # Markel 
    # Also save the duration column from the raw trace
    #tracebuf = read_tracebuf(tracefile, workername, loggerq)
    tracebuf, timingbuf = read_tracebuf(tracefile, workername, loggerq)

    # Markel
    # Remove anything but the numbers from the timings, keep the empty
    # lines to mantain line numbers
    timingbuf = timingbuf.split("\n") # TODO this might not work, does
                                      #      columns[1] include \n?
    timingbuf = [re.search(r'\d+\.?\d+', elem) for elem in timingbuf]
    timingbuf = [elem.group() if elem != None else elem for elem in timingbuf]

    lexer = lex.lex(module=ftracelexer.Ftracelexer(searchlist, \
                                                   workername, \
                                                   loggerq))
    lexer.async_nesting = collections.defaultdict(int)
    lexer.async_balance = collections.defaultdict(list)
    lexer.num_braces = collections.defaultdict(int)
    lexer.trace_state = collections.defaultdict(str)
    lexer.currentpid = "NONE"
    lexer.input(tracebuf)

#	The next step is to parse the tokenized output. This is done in
#	a simple state machine:
#
#         START
#          |
#          v
#	+--------+  CALLSTART  +----------+  ASYNCSTART +-------------+
#       |        |------------>|          |------------>|             |
#       | INITIAL|             |IN_SYSCALL|             | ASYNC_EVENT |
#       |        |<------------|          |<------------|             |
#	+--------+  CALLEND    +----------+  ASYNCEND   +-------------+
#
#	This statemachine is implemented inside of the lexer! only the
#	tracing data gather in IN_SYSCALL is returned as well as the
#	start/end of meta-data.

    found_list = []
    nrspaces = 0
    current_pid = 'NONE'
    flist = collections.defaultdict(dict)
    while True:
        tok = lexer.token()
        if not tok:
            break
        else:
            if tok.type == "CALLSTART":
                fp_tmeta.write("S:{0}:{1}:{2}\n".format(current_pid, \
                                                        tok.lineno, \
                                                        tok.value))

                tout_buf = collections.defaultdict(list)
                meta_buf = collections.defaultdict(list)
                tout_buf[current_pid].append("{0}\n".format(tok.value))
                meta_buf[current_pid].append("{0}\n".format(testname))

                tout_fname, \
                timing_fname, \
                meta_fname = generate_filenames(tok.value[:-2], hostname)

                flist[current_pid]['tout_fname'] = tout_fname
                flist[current_pid]['timing_fname'] = timing_fname
                flist[current_pid]['meta_fname'] = meta_fname

            elif tok.type == "CALLEND":
                if flist[current_pid]:
                    write_outfile(flist[current_pid]['tout_fname'], \
                                  tout_buf[current_pid])

                    thash = md5(flist[current_pid]['tout_fname'])
                    meta_buf[current_pid].append("TRACE HASH: {0}\n".format( \
                                                                        thash))
                    
                    # Markel
                    # When a syscall end is detected, its duration is written
                    # Seems like lineno starts from 1, not 0, so we need -1
                    meta_buf[current_pid].append("DURATION: {0} us\n".format( \
                                                        timingbuf[tok.lineno-1]))

                    write_outfile(flist[current_pid]['meta_fname'], \
                                  meta_buf[current_pid])

                    found_list.append(flist[current_pid]['tout_fname'])

                    fp_tmeta.write("E:{0}:{1}:{2} duration: {3} us\n".format(current_pid, \
                                                            tok.lineno, \
                                                            thash, \
                                                            timingbuf[tok.lineno-1]))

                    # Remove all data of this PID!
                    flist.pop(current_pid)
                    tout_buf.pop(current_pid)
                    meta_buf.pop(current_pid)

            elif tok.type == "CALLSTARTEND":
                fp_tmeta.write("S:{0}:{1}:{2}\n".format(current_pid, \
                                                        tok.lineno, \
                                                        tok.value))

                tout_fname, \
                timing_fname, \
                meta_fname = generate_filenames(tok.value[:-3], hostname)

                flist[current_pid]['tout_fname'] = tout_fname
                flist[current_pid]['timing_fname'] = timing_fname
                flist[current_pid]['meta_fname'] = meta_fname

                write_outfile(flist[current_pid]['tout_fname'], \
                              tok.value)

                thash = md5(flist[current_pid]['tout_fname'])

                write_outfile(flist[current_pid]['meta_fname'], \
                              "TRACE HASH: {0}\nDURATION: {1} us\n".format(thash, timingbuf[tok.lineno-1]))

                # Markel
                # When a syscall end is detected, its duration is written
                # Seems like lineno starts from 1, not 0, so we need -1

                found_list.append(flist[current_pid]['tout_fname'])
                fp_tmeta.write("E:{0}:{1}:{2} duration: {3} us\n".format(current_pid, \
                                                        tok.lineno, \
                                                        thash, \
                                                        timingbuf[tok.lineno-1]))
                flist.pop(current_pid)

            elif tok.type == "TLINE":
                tout_buf[current_pid].append("{0}{1}\n".format(" "*2*nrspaces, \
                                                               tok.value))
            elif tok.type == "ASYNCSTART":
                meta_buf[current_pid].append("AS:{0}, {1}\n".format( \
                                                                tok.lineno, \
                                                                tok.value))
                fp_tmeta.write("AS:{0}, {1}\n".format(tok.lineno, \
                                                      tok.value))
            elif tok.type == "ASYNCEND":
                meta_buf[current_pid].append("AE:{0}\n".format(tok.lineno))
                fp_tmeta.write("AE:{0}\n".format(tok.lineno))

            elif tok.type == "INTSTART":
                meta_buf[current_pid].append("IS:{0}\n".format(tok.lineno))
                fp_tmeta.write("IS:{0}\n".format(tok.lineno))

            elif tok.type == "INTEND":
                meta_buf[current_pid].append("IE:{0}\n".format(tok.lineno))
                fp_tmeta.write("IE:{0}\n".format(tok.lineno))

            elif tok.type == "TSWITCH":
                fp_tmeta.write("T:{0}: {1}\n".format(tok.lineno, \
                                                     tok.value))

            elif tok.type == "TPID":
                current_pid = tok.value

            # update identation of ftrace data
            if tok.type == "BRAOPEN" or tok.type == "BRACLOSE" \
               or tok.type == "CALLEND" or tok.type == "ASYNCEND":
                nrspaces = int(tok.value[1])

    return found_list

def strip_testname(tracefile):
    """
    The raw trace contains the name of the test-executable, remove this line
    and return the name of the test-executable.
    """
    with open(tracefile, "r") as f_raw:
        testname = f_raw.readline()
        with open("tmp_{0}".format(tracefile), "w") as tf_raw:
            for line in f_raw:
                tf_raw.write(line)

    os.system("mv -f tmp_{0} {0}".format(tracefile)) # -f depending on the folder permissions
    return testname
