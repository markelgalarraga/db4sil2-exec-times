"""
Helper functions to configure ftrace.
"""

import logger as l
import shutil
import os

class FtraceSession:
    """
    ftrace wrapper class.
    """
    def __init__(self, ftracepath, loggerq):
        self.ftracepath = ftracepath
        self.loggerq = loggerq

    def open_ftrace_file(self, filename, writeable):
        """
        open ftracefile
        """
        try:
            if 1 == writeable:
                filep = open(filename, "w")
            else:
                filep = open(filename)
        except OSError as err:
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            return None

        return filep

    def set_ftrace_pid(self, pid):
        """
        Configure the PID which shall be traced.
        """
        retval = 0
        pidfile = "{0}/set_ftrace_pid".format(self.ftracepath)
        try:
            with self.open_ftrace_file(pidfile, 1) as fp_pid:
                fp_pid.write("{0}".format(pid))
        except OSError as err:
            l.log_a_msg("Failed to set ftrace PID to: {0}".format(pid), \
                        "ERROR", self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fp_pid.close()

        return retval

    def enable_tracing(self):
        """
        Start tracer.
        """
        retval = 0
        enablefile = "{0}/tracing_on".format(self.ftracepath)
        try:
            with self.open_ftrace_file(enablefile, 1) as fp_on:
                fp_on.write("1")
        except OSError as err:
            l.log_a_msg("Failed to enable ftrace.", "ERROR", \
                        self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fp_on.close()

        return retval

    def disable_tracing(self):
        """
        Stop tracing.
        """
        retval = 0
        enablefile = "{0}/tracing_on".format(self.ftracepath)
        try:
            with self.open_ftrace_file(enablefile, 1) as fp_on:
                fp_on.write("0")
        except OSError as err:
            l.log_a_msg("Failed to disable ftrace.", "ERROR", \
                        self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fp_on.close()

        return retval

    def set_tracer(self, newtracer):
        """
        configure the tracer that shall be used.
        """
        retval = 0
        ctracer = "{0}/current_tracer".format(self.ftracepath)
        try:
            with self.open_ftrace_file(ctracer, 1) as fp_ct:
                fp_ct.write(newtracer)
        except OSError as err:
            l.log_a_msg("Failed to switch to tracer: {0}".format(newtracer), \
                        "ERROR", self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fp_ct.close()

        return retval

    def set_trace_option(self, newoption):
        """
        Set a tracing option.
        """
        retval = 0
        tracer_options = "{0}/trace_options".format(self.ftracepath)
        try:
            with self.open_ftrace_file(tracer_options, 1) as fp_opt:
                fp_opt.write(newoption)
                fp_opt.close()
        except OSError as err:
            l.log_a_msg("Failed to set tracer option: {0}".format(newoption), \
                        "ERROR", self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1

        return retval

    def clear_trace_buffer(self):
        """
        Flush trace buffer.
        """
        retval = 0
        tracefile = "{0}/trace".format(self.ftracepath)
        try:
            with self.open_ftrace_file(tracefile, 1) as fp_trace:
                fp_trace.write(" ")
        except OSError as err:
            l.log_a_msg("Failed to clear trace buffer.", "ERROR", \
                        self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fp_trace.close()

        return retval

    def store_trace_to_file(self, filename, testname):
        """
        store ftrace result to filename.
        """
        retval = 0
        tracefile = "{0}/trace".format(self.ftracepath)
        try:
            with self.open_ftrace_file(tracefile, 0) as fp_trace:
                with open(filename, "w") as fout:
                    fout.write("{0}\n".format(testname))
                    for line in fp_trace:
                        fout.write(line)

            #shutil.copyfile(tracefile, filename)
            #cmd = 'cp {0} {1}'.format(tracefile, filename)
            #os.system(cmd)
        except OSError as err:
            l.log_a_msg("Failed to copy trace buffer to {0}.".format(filename),\
                        "ERROR", self.loggerq, "ftrace_session")
            l.log_a_msg(err, "ERROR", self.loggerq, "ftrace_session")
            retval = 1
        finally:
            fout.close()
            fp_trace.close()

        return retval

    def reset_ftrace(self):
        """
        Reset ftrace and flush the buffer.
        """
        self.disable_tracing()
        self.set_ftrace_pid(" ")
        self.clear_trace_buffer()

    def start_trace(self, pid):
        """
        Start tracing the process with pid.
        """
        self.set_ftrace_pid(str(pid))
        self.enable_tracing()

    def stop_and_store_trace(self, filename, testname):
        """
        stop ftrace and store resulting trace to filename.
        """
        self.disable_tracing()
        self.store_trace_to_file(filename, testname)
        self.reset_ftrace()
