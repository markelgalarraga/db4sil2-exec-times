"""
test_client.py -- the sensor of DB4SIL2 which runs the testexecutables
and collects the raw traces of these runs.
"""

import os
import time
import sys
import queue
import logger as l
import ftrace_session as fs
import db4sil2config as cfg
import socket
import datetime

# pylint does not seem to like the python3 print style, so disable the
# message triggered by the parentheses of print():
#pylint: disable=superfluous-parens

def perform_test(testexec, testtrace, host, port, loggerq):
    """
    perform one test, that is run one test-executable once.
    """

    # Wait for server to be ready first
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        while True:
            try:
                sock.connect((host, port))
                break
            except:
                #l.log_a_msg("Server not ready, waiting...", "ERROR", loggerq, "test_client")
                time.sleep(5)

    timeout = 600
    try:
        pid = os.fork()
    except OSError as err:
        l.log_a_msg("Unable to create child: {0}".format(err), "ERROR", \
                    loggerq, "test_client")
        sys.exit(1)

    if pid == 0:
        #  we in the child process so next we
        #     -> set up ftrace
        #     -> run the test executable
        child_pid = os.getpid()

        # it makes live a little easier to keep the process at one cpu
        #os.sched_setaffinity(0, [0])

        cmd = "echo {0} > /sys/fs/cgroup/cpuset/SIL2_A/tasks".format(child_pid)
        os.system(cmd)
        cmd = "echo {0} > /sys/fs/cgroup/devices/SIL2_A/tasks".format(child_pid)
        os.system(cmd)
        cmd = "echo {0} > /sys/fs/cgroup/palloc/SIL2_A/tasks".format(child_pid)
        os.system(cmd)



        # start ftrace
        #testtrace.start_trace(child_pid)
        l.log_a_msg("Running test: {0}.".format(testexec), "INFO", \
                    loggerq, "test_client")
        # just in case someone added some spaces in the tests file
        testexec = testexec.strip()
        cmd = testexec.split(' ')[0]
        args = testexec.split('/')[-1].split(' ')
        # use os.exec to run the subprocess in current shell
        os.execv(cmd, args)
        l.log_a_msg("Done.", "INFO", loggerq, "test_client")

    else:
        giveup_time = time.time() + timeout
        while os.waitpid(int(pid), os.WNOHANG) == (0,0):
            time.sleep(0.5)
            #if time.time() > giveup_time:
            #    cmd = "kill -9 {0}".format(pid)
            #    os.system(cmd)
            #    break
        # stop tracing and store trace
        #os.waitpid(int(pid), os.WUNTRACED)
        current_t = datetime.datetime.now()
        formatted_dt = current_t.strftime("%Y-%m-%d_%H%M%S%f")
        test_tracefile = "test_trace_{0}".format(formatted_dt)
        testtrace.stop_and_store_trace(test_tracefile, testexec)

        # Connect again, Linux has timed out the conn
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((host, port))
            sock.sendall(bytes("{0}".format(test_tracefile), "utf-8"))
            sock.recv(1024) # Wait for server close
            sock.close()

def test_client_code():
    """
    actual "main()"
    """

    logger_queue = queue.Queue()

    # Before we do anything else: start the logger thread, so we can log
    # whats going on
    threadid = 1
    logger_t = l.DB4SIL2Logger(threadid, "logger", logger_queue, \
                               "db4sil2_client.log")
    logger_t.daemon = True # make the logger thread a daemon, this way the main
                           # will clean it up before terminating!
    logger_t.start()

    texeclist, \
    tracepath, \
    numruns, \
    testmode, \
    allowed_backlog = cfg.get_test_config('db4sil2.cfg', \
                                          "test_client", \
                                           logger_queue)
    if [] == texeclist:
        sys.exit(-1)

    host, port = cfg.get_client_config('db4sil2.cfg', "test_client", \
                                       logger_queue)

    testtrace = fs.FtraceSession(tracepath, logger_queue)
    testtrace.reset_ftrace()
    testtrace.set_tracer("function_graph")
    testtrace.set_trace_option("funcgraph-proc")
    testtrace.set_trace_option("noirq-info")
    testtrace.set_trace_option("nofuncgraph-irqs")

    if testmode == "suiteiteration":
        for run in range(0, numruns):
            while len(os.listdir()) > allowed_backlog:
                l.log_a_msg("Waiting for backlog to be reduced", \
                          "INFO", logger_queue, "test_client")
                time.sleep(10)
            for texec in texeclist:
                l.log_a_msg("Run Number {0}; Test: {1}".format(run, texec), \
                          "INFO", logger_queue, "test_client")
                perform_test(texec, testtrace, host, port, logger_queue)
                time.sleep(1)
    elif testmode == "testiteration":
        for texec in texeclist:
            while len(os.listdir()) > allowed_backlog:
                l.log_a_msg("Waiting for backlog to be reduced", \
                          "INFO", logger_queue, "test_client")
                time.sleep(10)
            for run in range(0, numruns):
                l.log_a_msg("Run Number {0}; Test: {1}".format(run, texec), \
                          "INFO", logger_queue, "test_client")
                perform_test(texec, testtrace, host, port, logger_queue)
    else:
        print("Unknown Test Mode! Valid Test Modes are:")
        print(" -> testiteration   == each test is executed numruns times.")
        print(" -> suitetiteration == the whole testsuite is "
              "executed numruns times.")

    os.system('reboot')
    sys.exit(0)
### main ###
if __name__ == '__main__':
    test_client_code()
