#!/bin/bash

SYSCALLS="SyS_accept SyS_bind SyS_brk SyS_clone SyS_close SyS_connect SyS_execve SyS_exit_group SyS_faccessat SyS_fcntl SyS_fstat SyS_futex SyS_getcwd SyS_getdents64 SyS_geteuid SyS_getpid SyS_getrandom SyS_ioctl SyS_listen SyS_lseek SyS_mkdirat SyS_mmap SyS_mmap_pgoff SyS_mprotect SyS_munmap SyS_newfstatat SyS_openat SyS_pipe2 SyS_prlimit64 SyS_read SyS_readlinkat SyS_rt_sigaction SyS_rt_sigprocmask SyS_sched_getaffinity SyS_sched_get_priority_max SyS_sched_get_priority_min SyS_sched_yield SyS_sendto SyS_set_robust_list SyS_setsockopt SyS_set_tid_address SyS_socket SyS_statfs SyS_sysinfo SyS_uname SyS_wait4 SyS_write"

if [ $1 = "start" ]
then
   if [ -z "$2" ]
   then
      python3 test_server.py & # > std.out 2> std.err &
      echo $! > test_server.pid
   else
      while true
      do
        for S in $SYSCALLS; do
           mkdir -p syscalls/traces/$S
           mkdir -p syscalls/traces/$S/unique_traces
           mkdir -p syscalls/timings/$S
           mkdir -p syscalls/meta/$S
        done
        mkdir -p syscalls/meta/PER_TEST
        
	#bash connection2.sh &  echo $! > connection.pid

        python3 test_server.py --config-file $2  #> std.out 2> std.err &
        echo "App restart"
	#kill -9 $(cat connection.pid)
        sleep 30
      done
   fi
elif [ $1 = "stop" ]
then
   PID=$(cat test_server.pid)
   kill -9 $PID
   rm test_server.pid
   rm connection.pid
else
   echo "Usage: ./test_server.sh [start|stop]"
fi
