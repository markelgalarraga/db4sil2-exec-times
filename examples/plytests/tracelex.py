#!/usr/bin/python3
# ------------------------------------------------------------
# tracelex.py
#
# tokenizer for ftrace output
# ------------------------------------------------------------
import ply.lex as lex

# List of token names.   This is always required
tokens = (
   'CALLSTART',
   'BRAOPEN',
   'BRACLOSE',
   'COMMENT',
   'TLINE',
   'STARTASYNC',
)

# Regular expression rules for simple tokens
t_ignore_COMMENT = r'\/\*[a-z,A-Z,0-9,_, ,.]*\*\/'  # comments are discarded.
t_TLINE = r'[a-z,A-Z,0-9,_,.,\[,\], ]+\(\);?'
t_STARTASYNC = r'__do_page_fault\(\);?|__update_curr\(\);?|__compute_runnable_contrib\(\);?'

def t_CALLSTART(t):
    r'SyS_write\(\)|SyS_read\(\)|SyS_open\(\)'
    t.lexer.num_braces = 0
    return t

def t_BRAOPEN(t):
    r'\{'
    t.lexer.num_braces += 1
    t.value = (t.value, t.lexer.num_braces)
    return t

def t_BRACLOSE(t):
    r'\}'
    t.lexer.num_braces -= 1

    if 0 == t.lexer.num_braces:
        print("END OF SYSCALL")
    elif 0 > t.lexer.num_braces:
        t.lexer.num_braces = 0

    t.value = (t.value, t.lexer.num_braces)
    return t

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()
lexer.num_braces = 0

data = ''
with open('test_trace') as fp:
   for line in fp:
      data += line

# Give the lexer some input
lexer.input(data)

# Tokenize
while True:
    tok = lexer.token()
    if not tok: 
        break      # No more input
    print(tok)
