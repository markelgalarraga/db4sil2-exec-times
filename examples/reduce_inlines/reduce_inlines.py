#!/usr/bin/python3

import sys
import collections
from lib import ncc

### helper functions ###

def read_systemmap(filename):
    symtab = set()

    with open(filename) as fmap:
        for line in fmap:
            # just take function names + remove trailing '\n'
            symtab.add(line.split(" ")[2][:-1])

    return symtab # fust a list of functions in the System.map

### main ###
if __name__ == '__main__':

    funclist, funcs, rfuncs = ncc.read_ncc_file(sys.argv[1])

    symtab = read_systemmap(sys.argv[2])

    inline_funcs = []

    for f in funclist:
        if f not in symtab:
            inline_funcs.append(f)

    for i in inline_funcs:
        called_funcs = funcs[i]
        funclist.remove(i)
        del funcs[i]
        if len(called_funcs) > 0: # does this function call any further functions?
            for pc in funclist:
                if i in funcs[pc]:
                    for ctmp in called_funcs:
                        if ctmp != None:
                            funcs = ncc.addfunc(pc, ctmp, funcs)
                    funcs = ncc.rmfunc(pc, i, funcs)

    ncc.print_cfg(funcs)
