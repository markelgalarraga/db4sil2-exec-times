#include<stdio.h>
#include<unistd.h>
#include<errno.h>

static inline int safe_read_inline(int fd, char *buff, size_t len){
	int pos = 0;

	do {
		errno=0;
		pos = read(fd, buff+pos, len);
		if(-1==pos){
			if(EINTR!=errno){
				return 0;
			}
		}

	} while(EINTR==errno);

	return pos;

}

int safe_read(int fd, char *buff, size_t len){
	int pos = 0;

	do {
		errno=0;
		pos = read(fd, buff+pos, len);
		if(-1==pos){
			if(EINTR!=errno){
				return 0;
			}
		}

	} while(EINTR==errno);

	return pos;

}


int main(int argc, char **argv)
{
	char msg[1000]={'\0'};
	int pos;

	pos = safe_read(STDIN_FILENO,msg,10);
	printf("pos: %d\n", pos);
	printf("message: %s\n", msg);
	
	pos = safe_read_inline(STDIN_FILENO,msg,10);
	printf("pos: %d\n", pos);
	printf("message: %s\n", msg);

	return 0;
}
