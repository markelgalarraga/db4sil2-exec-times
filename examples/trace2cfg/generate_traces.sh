#!/bin/bash

SYSCALLS="SyS_access SyS_execve      sys_getuid           SyS_mq_timedsend  SyS_read               SyS_sched_get_priority_max  SyS_timer_create
SyS_alarm            SyS_exit        SyS_kill             SyS_msync         sys_restart_syscall    SyS_sched_get_priority_min  SyS_timer_delete
sys_arch_prctl       SyS_exit_group  SyS_link             sys_munlockall    SyS_rt_sigaction       SyS_sched_getscheduler      SyS_timer_gettime
SyS_brk              SyS_fcntl       SyS_lseek            SyS_munmap        SyS_rt_sigpending      SyS_sched_setscheduler      SyS_timer_settime
SyS_clock_getres     SyS_fsync       SyS_madvise          SyS_nanosleep     SyS_rt_sigprocmask     sys_sched_yield             SyS_times
SyS_clock_gettime    SyS_ftruncate   SyS_mmap             SyS_newfstat      SyS_rt_sigqueueinfo    SyS_setitimer               SyS_unlink
SyS_clock_nanosleep  SyS_futex       SyS_mmap_pgoff       SyS_newlstat      sys_rt_sigreturn       SyS_setresuid               SyS_wait4
SyS_clock_settime    SyS_getdents    SyS_mprotect         SyS_newstat       SyS_rt_sigsuspend      SyS_set_robust_list         SyS_write
SyS_clone            SyS_getitimer   SyS_mq_getsetattr    SyS_open          SyS_rt_sigtimedwait    SyS_set_tid_address         SyS_mlock
SyS_close            sys_getppid     SyS_mq_open          SyS_openat        SyS_sched_getaffinity  SyS_statfs
SyS_dup              SyS_getrlimit   SyS_mq_timedreceive  SyS_pipe          SyS_sched_getparam     SyS_tgkill sys_open SyS_ioctl sys_geteuid"

for S in $SYSCALLS; do
   python3 trace2cfg.py ../../src/syscalls/traces/$S/unique_traces/ > cfg_ftrace_$S.txt
done


