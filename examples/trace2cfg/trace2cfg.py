#!/usr/bin/python3

import sys
import os
import collections
from lib import ncc

def tracetocfg(cfg, tracefile):
    levels = []

    with open(tracefile) as fin:
        for line in fin:
#          print("Handling line: {0}".format(line[:-1]))

            cur_spaces = line.count(" ")

            if len(levels) == 0:
                levels.append(line.strip())
            elif int(cur_spaces/2) + 1 == len(levels):
                levels.pop()
                levels.append(line.strip())
                cfg[levels[len(levels)-2]].add(line.strip())
            elif int(cur_spaces/2) + 1 > len(levels):
                cfg[levels[len(levels)-1]].add(line.strip())
                levels.append(line.strip())
            elif int(cur_spaces/2) + 1 < len(levels):
                while int(cur_spaces/2) != len(levels):
                    levels.pop()
                levels.append(line.strip())
                cfg[levels[len(levels)-2]].add(line.strip())

### main ###
if __name__ == '__main__':

    outcfg = collections.defaultdict(set)

    for f in os.listdir(sys.argv[1]):
        tracetocfg(outcfg, sys.argv[1]+f)

    ncc.print_cfg(outcfg)
