#!/bin/bash
##
## Usage: sudo ./ftrace1.sh /bin/true >trace

## set -o xtrace
set -o errexit
set -o pipefail

test $EUID -eq 0

D=/sys/kernel/debug/tracing

ftrace_exec() {
	echo $BASHPID >$D/set_ftrace_pid
	echo 1 >$D/tracing_on
	exec "$@"
}

## prepare
taskset --pid 1 $BASHPID >/dev/null
echo 0 >$D/tracing_on
echo function_graph >$D/current_tracer
echo funcgraph-proc >$D/trace_options
echo nofuncgraph-irqs >$D/trace_options
echo nofuncgraph-tail >$D/trace_options
echo nofuncgraph-overhead >$D/trace_options
true >$D/trace

for i in $(seq 1)
do
	ftrace_exec "$@" >/dev/null &
	child=$!
	wait

	echo 0 >$D/tracing_on
	cat $D/trace
	true >$D/trace
done

## cleanup
echo nop >$D/current_tracer

exit 0
