#!/usr/bin/python
## Usage: ./analyze.py </sys/kernel/debug/tracing/trace

import hashlib
import re
import sys

class funcgraph:
    DB = { "keys" }

    def __store(self, fn, body):
        if len(body) == 0:
            return fn
        val = ' '.join(body)
        key = fn + "_" + hashlib.md5(val).hexdigest()
        if key not in self.DB:
            self.DB.add(key)
            print(key + ": " + val)
        return key

    STACK = [["root"]]

    def __enter(self, fn):
        self.STACK.append([fn])

    def __leave(self):
        func = self.STACK.pop()
        key = self.__store(func[0], func[1:])
        self.STACK[-1].append(key)

    bad_chars = re.compile('[^.0-9A-Z_a-z]')
    EXCLUDE = { "__do_page_fault", "interrupt" }
    exclude = 0

    def enter(self, fn):
        fn = self.bad_chars.sub('_', fn)
        if self.exclude == 0:
            self.__enter(fn)
            self.exclude += fn in self.EXCLUDE
        else:
            self.exclude += 1

    def leave(self):
        if self.exclude > 0:
            self.exclude -= 1
        if self.exclude == 0:
            self.__leave()

fg = funcgraph()

for line in sys.stdin:
    if line.startswith('#'):
        continue

    if " ==========> " in line:
        fg.enter("interrupt")
        continue

    if " <========== " in line:
        fg.leave()
        continue

    stmt = line.split('|')[2].lstrip().rstrip()

    if stmt.endswith("() {"):
        fg.enter(stmt[:-4])

    elif stmt.endswith("();"):
        fg.enter(stmt[:-3])
        fg.leave()

    elif stmt.startswith('}'):
        fg.leave()

    else:
        assert False

assert fg.STACK[0][0] == "root"
assert fg.STACK[1][0] == "SyS_exit_group"
