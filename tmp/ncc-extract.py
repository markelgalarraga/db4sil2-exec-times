#!/usr/bin/python
## Usage: ./ncc-extract.py <code.map.nccout | LC_ALL=C sort | uniq --count | gzip >NCC-DB.gz

## extract caller-callee information from ncc-out data, looking like this:
## D: caller
## F: callee
## in order to produce a more machine-readable output like:
## callerA: callee1 callee2 ...
## callerB: callee1 callee3
## there are several other fields we are not interested in. furthermore, this
## is context sensitive: caller-blocks start with D and contain [FgGsS] fields.

## needed for E() print-error function
from __future__ import print_function
import re
import sys

bad_chars = re.compile('[^()*./0-9A-Z\[\]_a-z]')

def E(string):
    print(string, file=sys.stderr)

## parse ncc data block, containing several lines with tags
def block(NR, header, tags):
    F = []
    for line in sys.stdin:
        NR += 1
        if line[0] not in tags:
            break
        if line[0] == 'F':
            assert line.startswith('F: ') and line.endswith('()\n')
            f = line[3:-3]
            if bad_chars.search(f):
                ## f = bad_chars.sub('_', f)
                E("WARNING1 line %d: %s" % (NR, line.rstrip()))
            F.append(f)
    if header[0] == 'D':
        assert header.startswith('D: ') and header.endswith('()\n')
        d = header[3:-3]
        if bad_chars.search(d):
            E("WARNING2 line %d: %s" % (NR, header.rstrip()))
        print("%s: %s" % (d, ' '.join(F)))
    if line != '\n' and not line.startswith('#'):
        E("ERROR2 line %d: %s" % (NR, line.rstrip()))
    return NR

## keep track of line number
NR = 0
for line in sys.stdin:
    NR += 1
    if line == '\n' or line.startswith('#') or line.startswith('R: '):
        continue
    if line.startswith('D: '):
        NR = block(NR, line, 'FgGsS')
    elif line.startswith('P: '):
        NR = block(NR, line, 'YL')
    else:
        E("ERROR1 line %d: %s" % (NR, line.rstrip()))
